# Floating Labels in CSS
*Source: [css-tricks.com/float-labels-css/](http://css-tricks.com/float-labels-css/)*

## Resources

*transition-property:* [MDN transition-property](https://developer.mozilla.org/en-US/docs/Web/CSS/transition-property)  
*transition:* [MDN transition](https://developer.mozilla.org/en-US/docs/Web/CSS/transition)