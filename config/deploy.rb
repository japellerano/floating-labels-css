# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'form-labels'
set :scm, :git
set :repo_url, 'https://japellerano@bitbucket.org/japellerano/floating-labels-css.git'
set :deploy_to, '/var/deploy/form-labels'
set :deploy_via, :copy

set :ssh_options, {
  forward_agent: true,
  keys: '~/Downloads/ubuntu-ec2.pem'
}

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do

  task :symlink_dir do
    run "ln -sf /var/deploy/form-labels/current/ /var/www/experiments/form-labels"
  end

end